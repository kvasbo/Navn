/*import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('navn', () => App);
*/
import { Navigation } from 'react-native-navigation';
import { registerScreens } from './src/screenRegister';

registerScreens(); // this is where you register all of your app's screens

Navigation.startSingleScreenApp({
  screen: {
    screen: 'navn.PageLogin', // unique ID registered with Navigation.registerScreen
    title: 'Welcome', // title of the screen as appears in the nav bar (optional)
    navigatorStyle: {
      navBarHidden: true,
    }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
    navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
  }
});