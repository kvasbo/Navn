import { Navigation } from 'react-native-navigation';

import PageLogin from './screens/pageLogin';
import PageCards from './screens/pageCards';

export function registerScreens() {
  Navigation.registerComponent('navn.PageLogin', () => PageLogin);
  Navigation.registerComponent('navn.PageCards', () => PageCards);
}