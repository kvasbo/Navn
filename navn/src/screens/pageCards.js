import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Swiper from 'react-native-deck-swiper';
import Firebase from 'react-native-firebase';

const db = Firebase.firestore();

export default class PageCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loaded: false, names: [] }
  }

  componentDidMount() {
    this.initNames();
  }

  initNames() {
    db.collection('names').get()
    .then((snapshot) => {
        const names = [];
        snapshot.forEach((doc) => {
          names.push(doc.data().name)
        });
        console.log(names);
        this.setState( { names, loaded: true });
    })
    .catch((err) => {
        console.log('Error getting documents', err);
    });
  }

  render () {

    if(!this.state.loaded) {
      return <View><Text>Laster</Text></View>;
    }

    return (
    <View style={styles.container}>
        <Swiper
            cards={this.state.names}
            renderCard={(card) => {
                return (
                    <View style={styles.card}>
                        <Text style={styles.text}>{card}</Text>
                    </View>
                )
            }}
            onSwiped={(cardIndex) => {console.log(cardIndex)}}
            onSwipedAll={() => {console.log('onSwipedAll')}}
            cardIndex={0}
            backgroundColor={'#4FD0E9'}>
        </Swiper>
    </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent'
  }
})