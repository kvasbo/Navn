import React from 'react';
import { View, StyleSheet, Button } from 'react-native';
import firebase from 'react-native-firebase'

export default class PageLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false,
    };
  }

  componentDidMount() {
    this.unsubscribe = firebase.auth().onUserChanged(this.onUserChanged);
    this.doAnonLogin();
  }

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  onUserChanged = (currentUser) => {
    if (currentUser) {
      console.log(currentUser.toJSON())
    }
  }

  doAnonLogin = () => { firebase.auth().signInAnonymously()
    .then(() => {
      this.setState({
        isAuthenticated: true,
      });
    });
  }

  render() {
    return (
      <View style={pageStyles.container}>
        
      </View>
    );
  }
}

const pageStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingLeft: 0,
    paddingTop: 0,
    paddingBottom: 30,
    paddingRight: 0,
  },
});
